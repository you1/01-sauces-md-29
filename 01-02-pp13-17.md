---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}-->









### 基本ソースのレシピ {#grandes-sauces-de-base}

<!--<div class="frsecenv">Grandes Sauces de Base</div>-->


\index{そす@ソース!きほん@基本---|(}
\index{sauce@sauce!grandes@Grandes ---s de Base|(}


<div class="recette">

#### ソース・エスパニョル[^0102_08]  {#sauce-espagnole}

<div class="frsubenv">Sauce espagnole</div>

\index{そす@ソース!えすはによる@---・エスパニョル}
\index{えすはによる@エスパニョル!そす@ソース・---} 
\index{すへいんふう@スペイン風（エスパニョル）!そすえすはによる@ソース・エスパニョル}
\index{sauce@sauce!espagnole@--- Espagnole}
\index{espagnol@espagnol!sauce@Sauce ---e}


（仕上がり5 L分）


**[リエゾン\*](#lier-gls)用の[ルー](#roux-brun)**……625 g。

[**茶色いフォン**](#fonds-brun-ou-estouffade)（ソースを仕上げるのに必要な全量）……12 L。

[**ミルポワ\***](#mirepoix-gls)（香味素材）[^102010]……小さな[デ\*](#des-gls)に切った塩漬け豚
ばら肉150 g、大きめの[ブリュノワーズ\*](#brunoise-gls)に切ったにんじん250 gと玉ねぎ
150 g、タイム2枝、ローリエの葉（小）2枚。\index{みるほわ@ミルポワ}\index{mirepoix}

**作業手順**

1° フォン8 Lを鍋で沸かす。あらかじめ柔らかくしておいたルーを加え、木杓
   子か泡立て器で混ぜながら沸騰させる。

   弱火にして[^0102001]微沸騰の状態を保つ。


2° 以下のようにしてあらかじめ用意しておいたミルポワを投入する。ソテー
   鍋に塩漬け豚ばら肉を入れて火にかけて脂を溶かす。そこに、[ブリュノワー
   ズ*](#brunoise-gls)に刻んだにんじんと玉ねぎ、タイム、ローリエの葉を
   加える。野菜が軽く色づくまで強火で炒める。丁寧に、余分な脂を捨てる。
   これをソースに加える。野菜を炒めたソテー鍋に白ワイン約100
   mL[^0102_25]を加えて[デグラセ\*](#deglacer-gls)し、半量まで煮詰める。
   これも同様にソースの鍋に加える。こまめに[デプイエ
   \*](#depouiller-gls)しながら弱火で約1時間煮込む。


3° ソースを[シノワ\*](#chinois-gls)で、ミルポワ野菜を軽く押しながら漉し、別の
   片手鍋に移す。フォン2 Lを注ぎ足す。さらに2時間、微沸騰の状態を保ち
   ながら加熱する。その後、陶製の鍋に移し、ゆっくり混ぜながら冷ます。


4° 翌日、再び厚手の片手鍋に移してから、フォン2 Lとトマトピュレ1 Lまた
   は同等の生のトマトつまり2 kgを加える。トマトピュレを用いる場合は、
   あらかじめオーブンでほとんど茶色になるまで焼いておくといい。そうす
   るとトマトピュレの酸味を抜くことが出来る。そうすればソースを澄ませ
   る作業が楽になるし、ソースの色合いも温かそうで美しいものになる。ソー
   スをヘラか泡立て器で混ぜながら強火で沸騰させる。弱火にして1時間微沸
   騰の状態を保つ。最後に、細心の注意を払いながらデプイエする。布で\ruby{漉}{こ}
   し、完全に冷めるまで、ゆっくり混ぜ続けること[^3bis]。


###### 【原注】 {#nota-sauce-espagnole}

ソース・エスパニョルで[デプイエ\*](#depouiller-gls)にかかる時
間はいちがいには言えない。これは、ソースに用いるフォンの質次第で変わる
からだ。


ソースにするフォンが上質なものであればある程、デプイエの作業は早くすむ。
そういう場合には、ソース・エスパニョルを5時間で作ることも無理ではない。


[^0102001]: 原文から直訳すると「鍋を火の脇に置く」だが、21世紀の調理環境
    では単純に「弱火にする」と解釈していい。


[^3bis]: この、ヘラなどでゆっくり混ぜながら冷ます作業を vanner （ヴァ
    ネ）と呼ぶが、日本の調理現場ではあまり用いられていない。



[^0102_08]: 本節冒頭では、ルーがスペインの料理人によってもたらされ、そ
    の結果としてソース・エスパニョルが作られるようになったと読める記述
    があるが、これはむしろ誤りと考えるべき。エスパニョル espagnol(e)は
    「スペイン（風）の」意だが、スペイン料理起源というわけでもない。ス
    ペインを想起させるトマトを使うから、あるいは、ソースが茶褐色なのが
    ムーア系スペイン人を想起させるから、など定説はない。カレーム『19世
    紀フランス料理』第3巻に収められたソース・エスパニョルの作り方は、
    フォンをとるところから始まり4ページにわたって詳細なものとなってい
    る（pp.8-11）。その中で、肉を入れた鍋に少量のブイヨンを注いで煮詰
    めることを繰り返す。ここまでは18世紀の料理書で一般的な手法であるが、
    その後に大量のブイヨンを注いだ後、いきなり強火にかけるのではなく、
    弱火で加熱していくやり方を「スペイン式の方法」と述べている。カレー
    ムにおいては、これがソースの名称の根拠のひとつになっていると考えて
    いいだろう。もちろん、ソース・エスパニョルという名称のソースはカレー
    ム以前からあり、1806年刊のヴィアール『帝国料理の本』にもカレームの
    レシピより簡単だが、ほぼ同様のものが基本ソースとして収録されている。
    また、それ以前にもソース・エスパニョルに類する名称のソースはあった
    が、たとえば1739年刊ムノン『新料理研究』第2巻にある「スペイン風ソー
    ス」はかなり趣きが異なる（コリアンダーひと把みを加えるのが特徴的）。
    同じ料理名でも時代や料理書の著者によってまったく違う料理になってい
    ることは、食文化史において珍しいことではない。また、とりわけ料理名
    に地名、国名が冠されているものの中には根拠や由来のはっきりしないも
    のも多い。いずれにしても、本書のソース・エスパニョルの源流は19世紀
    初頭のヴィアールあたりからと考えられる。ソース・エスパニョルは19世
    紀を通して普及し、茶色いソースの代表的な存在となった。こんにちでも
    フォンドヴォーをベースとしたソースは、ルーでとろみ付けこそしないが、
    仔牛の骨などから出るコラーゲンによるとろみを利用したもので、仕上が
    りの色合いや、ごく標準的ともいえる風味付けの方法などが引継がれ続け
    ている調理現場も少なくない。もっとも、上述のように本書では「茶色い
    ルー」を使うところに「エスパニョル」であることの理由を見い出そうと
    していると解釈される。

[^0102_25]: 原文 un verre de vin blanc （アンヴェールドヴァンブロン）。
    直訳すると「グラス1杯の白ワイン」だが、本書において un verre de 〜
    は「約1 dL=100 mL」と覚えておくといいだろう。


[^102010]: ミルポワ (mirepoix) とはそもそもブリュノワーズに切った香味
    野菜と塩漬け豚ばら肉、およびそれを炒めたもののことをいい、それ自体
    が調味料的な性格をもつ。日本の調理現場では香味野菜全般を指す言葉と
    解釈されていることが多いが、フランス語とずれがあるので注意。[II.ガ
    ルニチュール、ミルポワ](#mirepoix)参照。




\atoaki{}

#### [小斉\*](#maigre-gls)用ソース・エスパニョル {#sauce-espagnole-maigre}


<div class="frsubenv">Sauce espagnole maigre</div>


\index{そす@ソース!えすはによるるさかな@---・エスパニョル (魚料理用)}
\index{えすはによる@エスパニョル!そすさかなよう@ソース・--- (魚料理用)}
\index{すへいんふう@スペイン風（エスパニョル）!そすえすはによるさかな@ソース・エスパニョル（魚料理用）}
\index{sauce@sauce!espagnole maigre@--- Espagnole maigre}
\index{espagnol@espagnol!sauce maigre@Sauce Espagnole maigre}


（仕上がり5 L分）


**バターを用いて[^0102007]作った[ルー](#roux-brun)**……500 g。


**[魚のフュメ](#fonds-ou-fumet-de-poisson)**（ソースを仕上げ
  るために必要な全量）……10 L。


**ミルポワ**……標準的な[ソース・エスパニョル](#sauce-espagnole)と同じ[ミルポワ\*](#mirepoix-gls)野菜を同
量と、塩漬け豚ばら肉の代わりにバターを用い、マッシュルームまたはマッシュ
ルームの切りくず[^0102_27]250 gを加える。


**作業手順**……標準的なソース・エスパニョルとまったく同様に作る。


**加熱と[デプイエ\*](#depouiller-gls)に必要な時間**……5時間。


仕上げに漉してから、標準的なソース・エスパニョルとまったく同様に、完全
に冷めるまでゆっくり混ぜ続けること。




<!--\atoaki{}-->

###### 小斉用ソース・エスパニョル補足 {#observation-sauce-espagnole-maigre}



……このソースを日常的な料理のベースとなる仕込みに含めるかどうかについては
意見が分れるところだ。


標準的な[ソース・エスパニョル](#sauce-espagnole)は、つまるところ風味の点ではほとんどニュート
ラルなものだから、それに[魚のフュメ](#fonds-ou-fumet-de-poisson)を加えれば、[小斉用ソース・エスパニョ
ル](#sauce-espagnole-maigre)として充分に通用するだろう。どうしても上で挙げた小斉用ソース・エス
パニョルが必要になるのは、宗教的に厳格に小斉の約束事を守って料理を作る
場合のみで、さすがにその場合は代用品などない。






[^0102007]: 初版〜第三版にかけては、茶色いルーを作るのに「バターまたは、
    きれいなグレスドマルミット（コンソメなどを作る際に表面に浮いてくる
    脂をすくい取って、不純物を漉し取ったものであり、基本的に獣脂）」を
    用いる、とある。上述のように、カトリックにおける「小斉」の場合、獣
    脂は忌避されたがバターなどの乳製品は許容された。そのため特に「バター
    を用いて作ったルー」という指定がなされ、第四版では茶色いルーに澄ま
    しバターのみを使う旨が強調されたが、ここでは初版以来の記述がそのま
    ま残っているために、やや冗長に思われる表現となっている。


[^0102_27]: champignons de Paris （ションピニョンドゥパリ）いわゆるマッ
    シュルームは、ガルニチュールなど料理の一部として提供する際に、トゥ
    ルネ tourner といって螺旋（らせん）状の切れ込みを入れて装飾し
    たものを使う。その際に少なくない量、具体的には重量で15〜20％程度が
    「切りくず」として発生するのでこれを利用する。なお、tourner（トゥ
    ルネ）の原義は「回す」であり、包丁を持った側の手は動かさずに、材料
    のほうを回すようにして切れ目を入れたり、アーティチョークや果物など
    の皮を剥くことを意味する。




\atoaki{}

#### ソース・ドゥミグラス[^0102_09]  {#sauce-demi-glace}

<div class="frsubenv">Sauce demi-glace</div>

\index{そす@ソース!とうみくらす@---・ドゥミグラス}
\index{とうみくらす@ドゥミグラス!そす@ソース・---}
\index{sauce@sauce!demi-glace@--- Demi-glace}
\index{demi-glace@demi-glace!sauce@sauce ---}



一般に「ドゥミグラス」と呼ばれているものは、いったん仕上がった[ソース・
エスパニョル](#sauce-espagnole)をさらに、もうこれ以上は無理という位に
徹底的に[デプイエ\*](#depouiller-gls)したもののことだ。



最後の仕上げに[グラスドヴィアンド](#glace-de-viande)などを加える。風味
付けに何らかの酒類[^0102_28]を加えれば、当然ながらソースの性格も変わる
ので、最終的な使い途に応じて決めること。





###### 【原注】 {#nota-sauce-demi-glace}

ソースの色合いを決めるワインを仕上げに加える際には、\kenten{火から外し
て}やること。沸騰しているとワインの香りがとんでしまうからだ。


[^0102_09]: 日本の洋食などで一般的な「デミグラス」あるいは「ドミグラス」」
    とはかなり異なった仕上りのソースであることに注意。ソース・エスパニョ
    ルの仕上げにあたって、徹底的に不純物を取り除くことを何度も強調して
    いるのは、透き通った茶色がかった色合いの、なめらかなソースを目指す
    からであり、それをさらに徹底させるということは、透明度、なめらかさ
    の面でさらに上を目指すということを意味するからだ。ちなみに、アメリ
    カに本社のあるメーカーの「デミグラスソース」の缶詰はもっぱら日本で販売
    されている製品であり、ヨーロッパおよびアメリカでは同一ブランドに該
    当する商品は存在しないようだ。

[^0102_28]: 本書ではマデイラ酒（マデイラワイン、ポルトガルの酒精強化ワ
    イン、すなわちブドウ果汁が酵母により醗酵している途中で蒸留酒を加え
    て醗酵を止める製法のもので、甘口のものが多い）が用いられることが多
    い。





\atoaki{}

#### とろみを付けた仔牛のジュ {#jus-de-veau-lie}


<div class="frsubenv">Jus de veau lié</div>


\index{しゆ@ジュ!こうしのしゆ@仔牛の---(とろみを付けた)}
\index{そす@ソース!とろみをつけたこうしのしゆ@とろみを付けた仔牛のジュ}
\index{こうし@仔牛!とろみをつけたこうしのしゆ@とろみを付けた---のジュ}
\index{jus@jus!jus veau lie@--- de veau lié}
\index{veau@veau!jus lie@jus de --- lié}
\index{jus de veau lie@Jus de veau lié}



（仕上がり1 L分）


**仔牛のフォン**……[仔牛の茶色いフォン](#fonds-ou-jus-de-veau-brun) 4 L。


**[リエゾン\*](#lier-gls)**……アロールート[^0102_10]30 g。


**作業手順**……よく澄んだ仔牛のフォン4 Lを強火にかけ、$\frac{1}{4}$ 量つ
まり1 Lになるまで煮詰める。


大さじ数杯分の冷たいフォンでアロールートを溶く。これを沸騰している鍋に
加える。1分程度だけ火にかけ続けたら、布で漉す。


###### 【原注】 {#nota-jus-de-veau-lie}

この、とろみを付けた仔牛のジュは、本書では頻繁に使う指示をしているが、
必ず、しっかりした味で透き通った、きれいな薄茶色に仕上げること。




[^0102_10]: allow-root クズウコンを原料とした良質のでんぷん。日本では
    入手が難しいこともあり、コーンスターチが用いられることがほとんどだ
    が、シンガポールなどの東南アジアでは普通に入手可能なようだ。





\atoaki{}

#### ヴルテ[^0102_13]（標準的な白いソース）  {#veloute-ou-sauce-blanche-grasse}

<div class="frsubenv">Velouté, ou sauce blanche grasse</div>



\index{うるて@ヴルテ!ひようひゆんてきなそすうるて@標準的なソース・---}
\index{そす@ソース!うるてひようひゆん@ヴルテ（標準的な）}
\index{ふるて@ブルーテ|see {ヴルテ}}
\index{veloute@velouté}
\index{veloute@velouté!sauce blanche grasse@--- ou sauce blanche grasse}



（仕上がり5 L分）


**[リエゾン\*](#lier-gls)**……バターを用いて作った[^0102_11][ブロンドの
  ルー](#roux-blond) 625 g。


**よく澄んだ[仔牛の白いフォン](#fonds-blanc-ordinaire)**……5 L。


**作業手順**……ルーをフォンに溶かし込む。フォンは冷たくても熱くても
いいが、フォンが熱い場合にはソースが充分なめらかになるよう注意して溶か
すこと。混ぜながら沸騰させる。微沸騰の状態を保ちながら、浮いてくる不純
物を完全に取り除いていく[^0102_12]。この作業はとりわけ細心の注意を払っ
て行なうこと。


**加熱と[デプイエ\*](#depouiller-gls)に必要な時間**……1時間半。


その後、ヴルテを布で漉す[^0102_19]。陶製の鍋に移してゆっくり混ぜながら
完全に冷ます。


[^0102_11]: [小斉用ソース・エスパニョル](#sauce-espagnole-maigre)、訳
    注参照。


[^0102_12]: dépouiller （[デプイエ\*](#depouiller-gls)）。こんにちの日
本の調理現場ではほぼ[エキュメ\*](#ecumer-gls) (écumer)と区別されなくなっている用語。ソー
スや煮込み料理を仕上げる際に、浮き上がってくる不純物を徹底的に取り除き、
目の細かい布などで漉すこと。原義は動物などの皮を剥ぐ、剥くことの意で、
野うさぎの皮を剥ぐ、うなぎの皮を剥く、という意味では現代の厨房でも用い
られているる。ソースの場合は表面に凝固した蛋白質や油脂の膜が出来、それ
を「剥ぐように」取り除くことから、あるいは表面に浮いてくる不純物を徹底
的に取り除いてきれいなソースに仕上げることを、動物の皮を剥いてきれいな
身だけにすることになぞらえて、この用語が用いられるようになったようだ。
なお、本書においてécumer（エキュメ）が単に浮いてくる泡やアクを取る、と
いう作業であるのに対して、dépouiller（デプイエ）は「徹底的に不純物を取
り除いて美しく仕上げる」という意味合いが込められている。現代では品種改
良や農法の変化によって野菜のアクも少なくなり、小麦粉も精製度の高いもの
を利用出来るなど、食材および調味料の多くで純度の高いものを使用する場合
がほとんどであり、このデプイエという作業は20世紀後半にはほとんど行なわ
れなくなり、écumer（エキュメ）という用語だけで済ませることがほとんど
（cf.辻静雄監訳『オリヴェ ソースの本』柴田書店、1970年、27〜28頁）。


[^0102_13]: velouté （ヴルテ）原義は「ビロードのように柔らかな、なめら
    かな」。日本ではベシャメルソースと混同されやすいが、内容がまったく
    異なるソースなので注意。


[^0102_19]: ある程度濃度のある液体やピュレを布で漉す場合、昔は「二人が
    かりで行なう必要があり、それぞれが巻いた布の端を左手に持ち、右手に
    持った木杓子を使って圧し搾る」（『ラルース・ガストロノミーク』初版、
    1938年）という方法が一般的だった。





\atoaki{}

#### 鶏のヴルテ  {#veloute-de-volaille}

<div class="frsubenv">Velouté de volaille</div>


\index{うるて@ヴルテ!とりのうるて@鶏の---（ヴルテドヴォライユ）}
\index{そす@ソース!うるてとり@ヴルテ（鶏）}

\index{うおらいゆ@ヴォライユ!うるてとうおらいゆ@ヴルテドヴォライユ（鶏のヴルテ）}
\index{かきん@家禽!とりのうるて@鶏のヴルテ}
\index{veloute@velouté!volaille@--- de Volaille}
\index{sauce@sauce!veloute volaille@Velouté de Volaille}



このヴルテの作り方だが、上述の[標準的なヴルテ](#veloute-ou-sauce-blanche-grasse)と、材料比率と
作業はまったく同じ。使用する液体として[鶏の白いフォン（フォンドヴォラ
イユ）](#fonds-de-volaille)を使う。





\atoaki{}

#### 魚料理用ヴルテ {#veloute-de-poisson}

<div class="frsubenv">Velouté de poisson</div>


\index{うるて@ヴルテ!さかなうるて@魚料理用---}
\index{そす@ソース!うるてさかな@ヴルテ(魚料理用)}
\index{veloute@velouté!poisson@--- de Poisson}
\index{sauce@sauce!veloute poisson@Velouté de Poisson}


ルーと液体の分量は[標準的なヴルテ](#veloute-ou-sauce-blanche-grasse)と
まったく同じだが、[仔牛のフォン](#fonds-blanc-ordinaire)ではなく[魚の
フュメ](#fonds-ou-fumet-de-poisson)を用いて作る。


ただし、魚を素材として用いるストックはどれもそうだが、手早く作業するこ
と。[デプイエ\*](#depouiller-gls)の作業も20分程度にとどめること。その後、布で漉し、陶
製の鍋に移してゆっくり混ぜながら完全に冷ます。





\atoaki{}

#### ソース・アルモンド、パリ風ソース[^102009] {#sauce-allemande}


<div class="frsubenv">Sauce parisienne (ex-Allemande) (1-4)</div>




\index{そす@ソース!ぱりふう@パリ風--- ⇒ ---・アルモンド}
\index{はりふう@パリ風!そす@---ソース ⇒ ---・アルモンド}
\index{といつふう@ドイツ風!そす@ソース・アルモンド}
\index{あるもん@アルモン（ド）!そす@ソース・アルモンド}
\index{sauce@sauce!parisienne@--- parisienne (ex-Allemande)}
\index{parisien@parisien!sauce@Sauce Parisienne = Sauce Allemande}
\index{allemand@allemand!sauce@Sauce allemande (--- Parisienne)}



（仕上がり1 L分）


[標準的なヴルテ](#veloute-ou-sauce-blanche-grasse)に卵黄で[リエ\*](#lier-gls)したソース。


**標準的なヴルテ**……1 L。


**追加素材**……卵黄5個、[白いフォン](#fonds-blanc-ordinaire)（冷たいもの） $\frac{1}{2}$ L、粗く
砕いたこしょう1ひとつまみ、[ラペ\*](#raper-gls)したナツメグ少々、マッシュルームの
煮汁2 dL、レモン汁少々。


**作業手順**……厚手のソテー鍋にマッシュルームの茹で汁と白いフォン、卵黄、
粗く砕いたこしょう、ナツメグ、レモン汁を入れる。泡立て器でよく混ぜ、そ
こにヴルテを加える。火にかけて沸騰させ、強火で $\frac{2}{3}$ 量になるま
で、ヘラで混ぜながら煮詰める。


ヘラの表面がソースでコーティングされる状態になるまで煮詰めたら、布で漉
す。


膜が張らないよう、表面にバターのかけらをいくつか載せてやり、湯煎にかけ
ておく。


**仕上げ**……提供直前に、バター100 gを加えて仕上げる。


###### 【原注】 {#nota-sauce-allemande}

ソース・アルモンド（ドイツ風）とも呼ばれるが、本書では「パリ風」の名称
を採用した。そもそも「アルモンド」というの名称に正当性がないからだ。習
慣としてそう呼ばれてきただけであって、明らかに理屈に合わない名称だ
[^0102_15]。1883年に雑誌「料理技術」に某タヴェルネ氏が寄せた記事
には、当時ある優秀な料理人がアルモンドなどという理屈に合わない名称を使
うのはやめたという話が出ている。


こんにち既に「パリ風ソース」の名称を採用している料理長もいる。そう呼ん
だほうが好ましいわけだが、残念なことにまだ一般的にはなっていない
[^0102_16]。

[^102009]: sauce allemande （ソースアルモンドゥ）。現行の原書では「パ
    リ風ソース（元ソース・アルモンド）」となっているが、初版から第三版
    まではソース・アルモンドの名称のみ。第四版においてエスコ
    フィエはドイツを想起させる、あるいはドイツの名称をかなり徹底して排
    除し、レシピの名称を変更している。これは第一次大戦直後の戦勝国であ
    るフランスの世相も反映されていると思われる。また、後述のように、こ
    んにちでもソース・アルモンドの名称のほうが一般的であるため、ここで
    はSauce Parisienneの訳語としてソース・アルモンドをあてる
    こととした。




[^0102_15]: エスコフィエは普仏戦争に従軍した経歴があり、ドイツ嫌いとし
    て知られていた。



[^0102_16]: エスコフィエの願いもむなしく、現代においてもソース・アルマ
    ンドの名称で定着している。この「全注解」においても以後は「ソース・
    アルモンド」と訳しているので注意されたい。なお、「ドイツ風」とい
    うソース名の由来について、ソースの淡い黄色がドイツ人に多い金髪を
    想起させるからだとカレームは述べている。



\atoaki{}

#### ソース・シュプレーム[^0102_23] {#sauce-supreme}

<div class="frsubenv">Sauce supême</div>



\index{そす@ソース!そすしゆふれむ@---・シュプレーム}
\index{しゆふれむ@シュプレーム!そす@ソース・---}
\index{sauce@sauce!supreme@--- Suprême}
\index{supreme@suprême!sauce@Sauce ---}



[鶏のヴルテ](#veloute-de-volaille)に生クリーム[^0102_17]を
加えて口あたりをより滑らかにしたもの[^102008]。ソース・シュプレームは、正しく作った場合
「白さの\ruby{際}{きわ}だったとても繊細な」仕上がりのものでなくてはいけな
い。


（仕上がり1 L分）


**鶏のヴルテ**……1 L。


**追加素材**……[鶏の白いフォン](#fonds-de-volaille) 1 L、マッシュルーム
の茹で汁1 dL、良質な生クリーム 2 $\frac{1}{2}$ dL。


**作業手順**……鍋に鶏のフォンとマッシュルームの茹で汁、鶏のヴルテを入れ
て強火にかけ、ヘラで混ぜながら、生クリームを少しずつ加え、煮詰めていく。
このヴルテと生クリームを煮詰めたものの分量は、上で示した仕上がり 1 Lの
ソース・シュプレームを作るには、 $\frac{2}{3}$ 量まで煮詰まっていなく
てはならない。


布で漉し、仕上げに 1 dLの生クリームとバター80 gを加えてゆっくり混ぜなが
ら冷ますと、丁度最初のヴルテと同量になる。

[^102008]: monter （[モンテ\*](#monter-gls)）。

[^0102_23]: suprême 原義は「至高の」だが、料理においてはしばしば鶏や鴨
    の胸肉、白身魚のフィレなどを意味する。また、このソースのように、と
    くに意味もなくこの名を料理につけられているケースも多い。


[^0102_17]: フランスの生クリームのうち、料理でよく使われるのは、日本の
    生クリームにやや近い「クレーム・フレッシュ・パストゥリゼ」（低温殺
    菌した生クリームで乳脂肪分30〜38％）のほか、「クレーム・フレッシュ・
    エペス」（低温殺菌後に乳酸醗酵させたもので日本で一般的な生クリーム
    より濃度がある）、「クレーム・ドゥーブル」（殺菌後に乳酸醗酵させた
    もので乳脂肪分40％程度でかなり濃度がある）などがある。




\atoaki{}

#### ベシャメルソース[^0102_20]  {#sauce-bechamel}


<div class="frsubenv">Sauce Béchamel</div>



\index{そす@ソース!へしやめる@ベシャメル---}
\index{へしやめる@ベシャメル!そす@---ソース}
\index{sauce@sauce!bechamel@--- Béchamel}
\index{bechamel@Béchamel (sauce)}





（仕上がり 5 L分）

**[白いルー](#roux-blanc)**……650 g。


**使用する液体**……\ruby{沸}{わ}かした牛乳5 L。


**追加素材**……白身で脂肪のない仔牛肉300 gを[デ\*](#des-gls)に切り、
玉ねぎ（小）2個は[シズレ\*](#ciseler-gls)する。タイム1枝、粗く砕いたこしょ
う1つまみ、[ラペ\*](#raper-gls)したナツメグ少々、塩25 gとバターを鍋に加え
て蓋をし、色付かないように弱火で[エチュヴェ\*](#etuver-gls)する。


**作業手順**……沸かした牛乳でルーを溶く。混ぜながら沸騰させる。ここに、
先にエチュヴェしておいた野菜と調味料、仔牛肉を加える。弱火で1時間加熱する。
布で漉し[^0102_21]、表面にバターのかけらをいくつか載せて膜が張らないようにする。[小斉\*](#maigre-gls)用として厳密に調理する必要がある場合は、
仔牛肉をはぶき、香味野菜などは上記のとおりに作る。


このソースは次の方法で手早くつくれる。沸かした牛乳に塩、薄切りにした玉
ねぎ、タイム、粗く砕いたこしょう、ナツメグを加える。蓋をして弱火で10分
加熱する。これを漉してルーを入れた鍋の中に入れ、強火にかけて沸騰させる。そ
の後15〜20分だけ煮込めばいい。


[^0102_20]: 17世紀にルイ14世のメートルドテルを務めたこともあるルイ・ベ
    シャメイユLouis Béchameil（1630〜1703）の名が冠されているこのソー
    スは、彼自身あるいは彼に仕えていた料理人によるものという説もあった
    が真偽は疑わしい。17世紀頃の成立であることは確かだが、19世紀前半の
    カレームのレシピはヴルテを煮詰め、卵黄と煮詰めた生クリームで[リエ\*](#lier-gls)するというものだった。同様に1867年刊グフェ『料理の本』のレシピ
    も、炒めた仔牛肉と野菜に小麦粉を振りかけてからブイヨンを注ぎ、これを
    煮詰め、漉してから生クリームを加えるというものだった。

[^0102_21]: [ヴルテ](#veloute-ou-sauce-blanche-grasse)訳注参照。





\atoaki{}

#### トマトソース  {#sauce-tomate}

<div class="frsubenv">Sauce tomate</div>

\index{そす@ソース!とまとそす@トマト---}
\index{とまと@トマト!ソース@---ソース}
\index{sauce@sauce!tomate@--- tomate}
\index{tomate@tomate!sauce@Sauce ---}


（仕上がり5 L分）


主素材……トマトピュレ4 L、または生のトマト6 kg。


[ミルポワ](#mirepoix-gls)……[デ\*](#des-gls)に切って[ブロンシール\*](#blanchir-gls)しておいた塩漬け
豚ばら肉 140 g、にんじん 200 gと玉ねぎ 150 gの[ブリュノワーズ\*](#brunoise-gls)、
ローリエの葉1枚、タイム 1 枝、バター 100 g。



追加素材……小麦粉 150 g、[白いフォン](#fonds-blanc-ordinaire) 2 L、にんにく（小） 2 片。


調味料……塩 20 g、砂糖 30 g、こしょう 1 つまみ。


作業手順……厚手の片手鍋で、塩漬け豚ばら肉を軽く[リソレ\*](#rissoler-gls)する。。
ミルポワの野菜を加え、野菜も色よく炒める。小麦粉を振りかける。ブロンド
色になるまで炒めてから[^102011]、トマトピュレまたは潰した生トマトと白いフォン、
すり潰したにんにく、塩、砂糖、こしょうを加える。


火にかけて混ぜながら沸騰させる。鍋に蓋をして弱火のオーブンに入れ1時間
半〜2時間加熱する。


目の細かい漉し器または布で漉す。再度、火にかけて数分間沸騰させる。保存
用の器に[デバラセ\*](#debarrasser-gls)し、ソースが空気に触れて表面に膜が張らないよう、バターのかけ
らを載せてやる。


###### 【原注】 {#nota-sauce-tomate}

トマトピュレを使い、小麦粉は使わず、その他は上記のとおりに作ってもいい。
漉し器か布で漉してから、充分な濃度になるまでしっかり煮詰めてやること。

[^102011]: 原文 lorsque celle-ci est revenue （ロスクセルスィエルヴニュ）直訳すれば、ミルポワが[ルヴニール\*](#revenir-gls)された状態になったら、の意。

\index{そす@ソース!きほん@基本---|)}
\index{sauce@sauce!grandes@Grandes ---s de Base|)}

</div><!--recette-->

<!--20190215江畑校正確認スミ-->
<!--20200304江畑校正確認スミ-->
